# FrontEnd.Validation
Custom jQuery validators and data annotation attributes

### How do I install it?
`install-package GreatCall.FrontEnd.Validation`

### How do I use it?
View the [wiki](https://bitbucket.org/greatcall/frontend.validation.wiki/wiki).
